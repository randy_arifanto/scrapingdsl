import time
start = time.time()

from sdsl.engine import Engine
import sys

# The source file is the 1st argument to the script
counterline = 0
scrapingDSL = ''''''
dsl = Engine()
if len(sys.argv) != 2:
    print('penggunaan: %s <src.sdsl>' % sys.argv[0])
    sys.exit(1)
with open(sys.argv[1], 'r') as file:
	for line in file:
		if not line or line[0] == '#' or line[0] == '\n':
			pass
		else:
			print line
			counterline += 1
			scrapingDSL = line

if counterline > 1:
	print("DSL tidak boleh lebih dari 1 baris")
else:
	result_path = "result/result_kompas.json"
	result_format = "json"
	dsl.run(scrapingDSL,result_format,result_path)

print 'It took', time.time()-start, 'seconds.'