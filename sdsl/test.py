from selenium import webdriver
from pyparsing import *
from parsers.parser import Parser
from lxml import etree
from classes.rule import Rule
import time

# driver = webdriver.Chrome()
# driver.get("https://lpse.pu.go.id/eproc/")
# time.sleep(5)
# try:
# next_komentar = driver.find_element_by_xpath('//div[@class="t-data-grid-pager"]/span[@class="current"]/following-sibling::a[position()=1]')
# element_scroll = driver.find_element_by_xpath('//div[@class="row mt2 clearfix"][last()]/h2')
# driver.execute_script("arguments[0].scrollIntoView(false);", next_komentar)
# next_komentar.click()
# except:
# 	pass

syntax_pair_val = [["BZURL","GET","BUGID"],["BZURL","GET","PRODUCT","COMPONENT"],["BZURL","GET","COMPONENT","PRODUCT"]]
n = len(syntax_pair_val)
if n > 1:
	total_jumlah = 0
	for x in xrange(n):
		for y in xrange((x+1),n):
			if sorted(syntax_pair_val[x]) == sorted(syntax_pair_val[y]):
				raise ValueError('DSL Driver Rule Error in syntax_pair should not be the same')
			else:
				pass