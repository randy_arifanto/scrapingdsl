from scraper.scraper.spiders import driver
from parsers.parser import Parser
from query.query_string import QueryString
from scraper.scraper.spiders import driver
from classes.rule import Rule
from classes.class_holder import ClassHolder
from classes.kwargsgen import KwargsGen
from classes.validator import Validator
import os

import scrapy
from scrapy.crawler import CrawlerProcess

class Engine(object):

	def run(self,query_arg,result_format,result_path):

		try:
			open(result_path, 'w').close()
		except:
			pass

		process = CrawlerProcess({
		    'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
		    'DOWNLOAD_DELAY' : 3,
			'FEED_FORMAT': result_format,
			'FEED_URI': result_path,
		})

		rule = Rule()
		rule_list = rule.rule_list()
		classes = ClassHolder()
		for p,v in rule_list.items():
			for j,k in v['item'].items():
				classes.add_class(eval(k['driver']))
				
		string_query = query_arg
		
		result = Parser()
		pars_object = result.parser(string_query)
		validator = Validator(string_query,pars_object,rule_list)
		validator.validate()
		kwargs_generator = KwargsGen(pars_object,rule_list[pars_object['category']]['item'][pars_object['driver']]['syntax'])
		kwargs = kwargs_generator.get_kwargs()
		# print kwargs

		driver_scraping = classes[rule_list[pars_object['category']]['item'][pars_object['driver']]['driver_class']] 
		process.crawl(driver_scraping, **kwargs)
		# process.crawl(driver.Download())
		process.start()

	def show_rule(self,kategory,driver_name):
		rule = Rule()
		rule_list = rule.rule_list()
		return rule_list[kategory]['item'][driver_name]

	def debug_function(self):

		process = CrawlerProcess({
		    'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
		    'DOWNLOAD_DELAY' : 3,
			'FEED_FORMAT': 'json',
			'FEED_URI': 'result_other/result.json',
			# 'FEED_URI': 'result/result_shopee_populer.json',
		})
		# process.crawl(driver.ShopeePopuler())
		process.crawl(driver.ShopeeKategori())
		process.start()