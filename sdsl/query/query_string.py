class QueryString(object):

	def __init__(self):
		"""Write your query in 'string' variable"""
		# self.string = '''GoogleResult: FROM google GET (gr_title,gr_description,gr_link,web_title) INKEYWORD "site:google" NPAGE "1"'''
		# self.string = '''eShop: FROM bukalapak GET (nama_barang,deskripsi,harga,kategori) INKEYWORD "iphone" NPAGE "1"'''
		# self.string = '''eShop: FROM tokopedia GET (nama_barang,harga,deskripsi,ulasan) INKEYWORD "baju" NPAGE "1"'''
		# self.string = '''eShop: FROM tokopedia GETBYPENJUAL (nama_barang_p,harga_p,deskripsi_p,ulasan_p) PENJUAL "hippopowerbank" ETALASE "semua" PENJUALNPAGE "2" ULASANNPAGE "2"'''
		# self.string = '''News: FROM detik GET (judul,tanggal,penulis,isi,komentar) INKEYWORD "jokowi" NPAGE "1" KOMENTARNPAGE "1"'''
		# self.string = '''News: FROM detik GET (judul,tanggal,penulis,isi) INKEYWORD "jokowi" NPAGE "2"'''
		# self.string = '''News: FROM detik GETBYFOKUS (judul,tanggal,penulis,isi) NFOKUS "2"'''
		self.string = '''News: FROM detik GETBYFOKUS (judul,tanggal,penulis,isi,komentar) NFOKUS "1" KOMENTARNPAGE "1"'''

	def dsl(self):
		return self.string