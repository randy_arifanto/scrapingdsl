from lxml import etree

class Validator(object):

	def __init__(self,dsl_query,parsed_object,rule):
		self.dsl_query = dsl_query
		self.parsed_object = parsed_object
		self.rule = rule

	def validate(self):

		# validasi penulisan syntax
		if 'syntax' not in self.parsed_object:
			raise TypeError('DSL Query Error in "'+self.dsl_query+'"')

		dic = self.rule[self.parsed_object['category']]['item'][self.parsed_object['driver']]

		# validasi category
		if self.parsed_object['category'] in self.rule:
			pass
		else:
			raise ValueError('DSL Query Error in "'+self.dsl_query+'"')

		# validasi driver
		for_valdriver = []
		for key_rule,value_rule in self.rule.items():
			for key_item,value_item in value_rule['item'].items():
				for_valdriver.append(key_item)
		if self.parsed_object['driver'] in for_valdriver:
			pass
		else:
			raise ValueError('DSL Query Error in "'+self.dsl_query+'"')

		# validasi syntax_pair -<tidak boleh sama>-
		syntax_pair_val = dic['syntax_pair']
		n = len(syntax_pair_val)
		if n > 1:
			total_jumlah = 0
			for x in xrange(n):
				for y in xrange((x+1),n):
					if sorted(syntax_pair_val[x]) == sorted(syntax_pair_val[y]):
						raise ValueError('DSL Driver Rule Error in syntax_pair should not be the same')
					else:
						pass

		# validasi apakah syntax sesuai dengan syntax pair dan optionalnya
		xml = self.parsed_object.asXML()
		doc = etree.fromstring(xml)
		btags = doc.xpath('//syntax')
		list_temp = []
		for item in btags:
			list_temp.append(item.text)

		for index,value in enumerate(dic['syntax_pair']):
			hasil = [item for item in list_temp if item not in value]
			if len(hasil) < len(list_temp):
				index_pair = index

		if index_pair in dic['optional_syntax']:
			for delete in dic['optional_syntax'][index_pair]:
				try:
					list_temp.remove(delete)
				except:
					pass

		if sorted(list_temp) == sorted(dic['syntax_pair'][index_pair]):
			pass
		else:
			print list_temp
			print dic['syntax_pair'][index_pair]
			raise ValueError('DSL Driver Rule Error in DSL Query does not match the syntax pair and its optional syntax')

		# validasi driver rule key
		list_driver_rule_key = []
		list_driver_rule_key_compare = ["driver","driver_class","syntax","syntax_pair","optional_syntax"]
		for key_driver_rule_key,value_driver_rule_key in dic.items():
			list_driver_rule_key.append(key_driver_rule_key)
		list_driver_rule_key = list(set(list_driver_rule_key))

		if len(list_driver_rule_key) == len(list_driver_rule_key_compare):
			compare_driver_rule_key = [c for c in list_driver_rule_key if c not in list_driver_rule_key_compare]
		else:
			if len(list_driver_rule_key) > len(list_driver_rule_key_compare):
				compare_driver_rule_key = [c for c in list_driver_rule_key if c not in list_driver_rule_key_compare]
			else:
				compare_driver_rule_key = [c for c in list_driver_rule_key_compare if c not in list_driver_rule_key]

		if len(compare_driver_rule_key) == 0:
			pass
		else:
			raise ValueError('DSL Driver Rule Error in Rule Key')
			
		# validasi syntax_pair & optional_syntax to syntax
		list_syntax = []
		for key_syntax,value_syntax in dic['syntax'].items():
			list_syntax.append(key_syntax)
		list_syntax = list(set(list_syntax))

		list_syntax_compare = []
		for value_syntax_pair in dic['syntax_pair']:
			for value_syntax_compare in value_syntax_pair:
				list_syntax_compare.append(value_syntax_compare)

		for key_syntax_opt,value_syntax_opt in dic['optional_syntax'].items():
			for value_opt in value_syntax_opt:
				list_syntax_compare.append(value_opt)
		list_syntax_compare = list(set(list_syntax_compare))

		if len(list_syntax) == len(list_syntax_compare):
			compare_syntax = [c for c in list_syntax if c not in list_syntax_compare]
		else:
			if len(list_syntax) > len(list_syntax_compare):
				compare_syntax = [c for c in list_syntax if c not in list_syntax_compare]
			else:
				compare_syntax = [c for c in list_syntax_compare if c not in list_syntax]

		if len(compare_syntax) == 0:
			pass
		else:
			raise ValueError('DSL Driver Rule Error in syntax_pair & optional_syntax should not be the same rule syntax')

		# Valid, so return True
		return True