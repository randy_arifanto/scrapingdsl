class Rule(object):

	def __init__(self):
		self.rule = {
			"eShop": {
				"item": {
					"tokopedia": {
						"driver": "driver.TokopediaResult",
						"driver_class": "TokopediaResult",
						"syntax": {
							"GET":{
								"nama_barang": False,
								"harga": False,
								"deskripsi": False,
								"ulasan": False,
							},
							"INKEYWORD": "keyword",
							"NITEM": "item_count",
							"GETBYPENJUAL":{
								"nama_barang_p": False,
								"ulasan_p": False,
								"harga_p": False,
								"deskripsi_p": False,
							},
							"PENJUAL": "penjual",
							"ETALASE": "etalase",
							"PENJUALNPAGE": "penjualpage",
							"ULASANNPAGE": "ulasanpage",
						},
						"syntax_pair": [["GET","INKEYWORD","NITEM"],["GETBYPENJUAL","PENJUAL","PENJUALNPAGE"]],
						"optional_syntax": {
							0:["ULASANNPAGE"],
							1:["ETALASE","ULASANNPAGE","NITEM"],
						},
					},
					"bukalapak": {
						"driver": "driver.BukalapakResult",
						"driver_class": "BukalapakResult",
						"syntax": {
							"GET":{
								"nama_barang": False,
								"deskripsi": False,
								"harga": False,
								"kategori": False,
							},
							"INKEYWORD": "keyword",
							"NITEM": "item_count",
						},
						"syntax_pair": [["GET","INKEYWORD","NITEM"]],
						"optional_syntax": {
						},
					}
				}
			},
			"News": {
				"item": {
					"detik": {
						"driver": "driver.DetikResult",
						"driver_class": "DetikResult",
						"syntax": {
							"GET":{
								"judul": False,
								"tanggal": False,
								"penulis": False,
								"isi": False,
								"komentar": False,
							},
							"INKEYWORD": "keyword",
							"NITEM": "item_count",
							"KOMENTARNPAGE": "komentar_page",
							"GETBYFOKUS":{
								"judul": False,
								"tanggal": False,
								"penulis": False,
								"isi": False,
								"komentar": False,
							},
							"NFOKUS": "jumlah_fokus",
						},
						"syntax_pair": [["GET","INKEYWORD","NITEM"],["GETBYFOKUS","NFOKUS"]],
						"optional_syntax": {
							0:["KOMENTARNPAGE"],
							1:["KOMENTARNPAGE"],
						},
					},
					"kompas": {
						"driver": "driver.KompasResult",
						"driver_class": "KompasResult",
						"syntax": {
							"GET":{
								"judul": False,
								"tanggal": False,
								"penulis": False,
								"isi": False,
								"komentar": False,
							},
							"INKEYWORD": "keyword",
							"NITEM": "item_count",
							"KOMENTARNPAGE": "komentar_page",
							"GETBYTAG":{
								"judul": False,
								"tanggal": False,
								"penulis": False,
								"isi": False,
								"komentar": False,
								"tag": False,
							},
							"TAGSLUG": "tag_slug",
							"NPAGEBYTAG": "page_count_tag",
						},
						"syntax_pair": [["GET","INKEYWORD","NITEM"],["GETBYTAG","TAGSLUG","NPAGEBYTAG"]],
						"optional_syntax": {
							0:["KOMENTARNPAGE"],
							1:["KOMENTARNPAGE","NITEM"],
						},
					}
				}
			},
			"Lelang": {
				"item": {
					"lpse": {
						"driver": "driver.LpseResult",
						"driver_class": "LpseResult",
						"syntax": {
							"GET":{
								"kode_lelang": False,
								"nama_lelang": False,
								"keterangan": False,
								"instansi": False,
								"satuan_kerja": False,
								"kategori": False,
								"metode_pengadaan": False,
								"metode_kualifikasi": False,
								"metode_dokumen": False,
								"metode_evaluasi": False,
								"anggaran": False,
								"nilai_pagu_paket": False,
								"nilai_hps_paket": False,
								"cara_pembayaran": False,
								"pembebanan_tahun_anggaran": False,
								"sumber_dana": False,
								"bobot_teknis": False,
								"bobot_biaya": False,
								"lokasi_pekerjaan": False,
								"syarat_kualifikasi": False,
							},
							"IDLPSE": "id_lpse",
							"INKEYWORD": "keyword",
							"NITEM": "item_count",
						},
						"syntax_pair": [["IDLPSE","GET","NITEM","INKEYWORD"]],
						"optional_syntax": {
						},
					}
				}
			},			
			"BugTracker": {
				"item": {
					"bugzilla": {
						"driver": "driver.BugzillaResult",
						"driver_class": "BugzillaResult",
						"syntax": {
							"BZURL": "bzurl",
							"GET":{
								"id": False,
								"nama_bug": False,
								"description": False,
								"comment": False,
								"status": False,
								"reported": False,
								"modified": False,
							},
							"INKEYWORD": "keyword",
							"NITEM": "item_count",
							"MAXCOMMENT": "max_comment",
						},
						"syntax_pair": [["BZURL","GET","INKEYWORD","NITEM"]],
						"optional_syntax": {
							0:["MAXCOMMENT"],
						},
					}
				}
			},			
			"Other": {
				"item": {
					"kpu": {
						"driver": "driver.Kpu",
						"driver_class": "Kpu",
						"syntax": {
							"GET":{
								"all": False,
							},
						},
						"syntax_pair": [["GET"]],
						"optional_syntax": {
						},
					}
				}
			},
		}

	def rule_list(self):
		return self.rule