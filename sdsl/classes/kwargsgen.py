from lxml import etree

class KwargsGen(object):

	def __init__(self,parsed_object,rule):
		self.parsed_object = parsed_object
		self.rule = rule

	def get_kwargs(self):
		item = {}
		for key,values in self.rule.items():
			if isinstance(values,dict):
				for keychild,valueschild in values.items():
					item.update({keychild:valueschild})
			else:
				item.update({values:None})
		xml = self.parsed_object.asXML()
		listvar = []
		doc = etree.fromstring(xml)
		btags = doc.xpath('//var')
		for b in btags:
			if b.xpath("./ITEM"):
				templistvar = []
				for c in b.xpath("./ITEM"):
					templistvar.append(c.text)
				listvar.append(templistvar)
			else:
				listvar.append(b.text)
		syntaxvar = []
		syntax = doc.xpath('//syntax')
		for syntaxitem in syntax:
			syntaxvar.append(syntaxitem.text)
		newdict = dict(zip(syntaxvar,listvar))
		item_from_query = {}
		for key,values in newdict.items():
			if isinstance(values,list):
				for valueschild in values:
					item_from_query.update({valueschild:True})
			else:
				item_from_query.update({self.rule[key]:values})
		item.update(item_from_query)
		return item