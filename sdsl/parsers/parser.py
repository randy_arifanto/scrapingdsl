from pyparsing import *

class Parser(object):

	def parser(self, string):
		colon			= Suppress(":")
		coma			= Suppress(",")
		openbracket		= Suppress("(")
		closebracket	= Suppress(")")
		fromquery		= Suppress("FROM")
		content			= Word(alphas+"_", alphanums+"_")
		syntax 			= Word(alphas).setResultsName("syntax")
		driver			= Word(alphas).setResultsName("driver")
		category		= Word(alphas).setResultsName("category") + colon
		stringvar		= quotedString.addParseAction(removeQuotes).setResultsName("var")
		booleanvar		= Group(openbracket + ZeroOrMore(Optional(coma) + content) + closebracket).setResultsName("var")
		syntaxvar		= ZeroOrMore(syntax + (stringvar | booleanvar))
		dsl				= category + fromquery + driver + syntaxvar

		parsed = dsl.parseString(string)
		return parsed