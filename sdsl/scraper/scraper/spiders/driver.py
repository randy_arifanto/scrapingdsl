import scrapy
import time
import urlparse
from selenium import webdriver
from scrapy import Selector
from abc import ABCMeta, abstractmethod

class IDriver:
	__metaclass__ = ABCMeta

	@abstractmethod
	def start_requests(self): raise NotImplementedError

	@abstractmethod
	def parse(self): raise NotImplementedError

class GoogleResult(scrapy.Spider,IDriver):

	name = "google"

	def start_requests(self):
		url = 'https://www.google.com/search?q='+self.keyword
		request = scrapy.Request(url=url, callback=self.parse)
		request.meta['counter'] = 0
		yield request

	def parse(self, response):

		alllink = response.xpath('//div[@class="g"]')

		if alllink is not None:

			nexturlxpath = response.xpath("//table[@id='nav']//tr/td[child::b]//following-sibling::td[position()=1]/a/@href").extract()
			halaman = response.xpath("//table[@id='nav']//tr/td/b/text()").extract_first()

			for link in alllink:
				foryield = {}
				gr_title = ''.join(link.xpath('./h3/a//text()[normalize-space()]').extract())
				gr_description = ''.join(link.xpath('.//span[@class="st"]//text()[normalize-space()]').extract())
				link_param = link.xpath('.//h3[@class="r"]/a/@href').extract_first()
				if link_param is not None:
					url = urlparse.urlparse(link_param)
					params = urlparse.parse_qs(url.query)
					if 'q' in params:
						url_params = params['q'][0]
						if "http" in url_params:        
							if self.gr_title == True:
								foryield.update({'gr_title': gr_title})
							if self.halaman == True:
								foryield.update({'halaman': halaman})	
							if self.gr_link == True:
								foryield.update({'gr_link': url_params})	
							if self.gr_description == True:
								foryield.update({'gr_description': gr_description})
							yield foryield
							# yield scrapy.Request(url=url_params, meta={'halaman': halaman, 'gr_title': gr_title, 'gr_description': gr_description}, callback=self.gettitle)

			if nexturlxpath:

				nexturl = ''.join(['https://www.google.com',nexturlxpath[0]])

				if response.meta['counter'] < (int(float(self.page_count)) - 1):
					request = scrapy.Request(url=nexturl, callback=self.parse)
					response.meta['counter'] += 1
					request.meta['counter'] = response.meta['counter']
					yield request

	def gettitle(self, response):        

		foryield = {}

		if self.gr_title == True:
			foryield.update({'gr_title': response.meta['gr_title']})

		if self.halaman == True:
			foryield.update({'halaman': response.meta['halaman']})	
				
		if self.gr_link == True:
			foryield.update({'gr_link': response.url})	

		if self.gr_description == True:
			foryield.update({'gr_description': response.meta['gr_description']})	

		if self.web_title == True:
			title = response.xpath("//title/text()").extract_first()
			if title is not None:
				foryield.update({'web_title': title})

		yield foryield

class BukalapakResult(scrapy.Spider):

	name = "bukalapak"

	def start_requests(self):
		urls = [
			'http://www.bukalapak.com',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse)
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		text_box = driver.find_element_by_xpath('//input[@id="search_keywords"]')
		submit_button = driver.find_element_by_xpath('//button[contains(@class, "c-omnisearch__button")]')
		text_box.send_keys(self.keyword)
		submit_button.click()
		time.sleep(10)
		nitem = int(float(self.item_count))
		while counter < 1:
			if nitem > 0:
				selenium_response_text = driver.page_source
				selector = Selector(text=selenium_response_text)
				allLink = selector.xpath('//a[contains(@class, "product__name") or contains(@class, "c-product-card__name")]/@href').extract()
				if allLink is not None:
					for link in allLink:
						if nitem > 0:
							if "http://www.bukalapak.com" in link:
								yield scrapy.Request(url=link, callback=self.parse_inside)
							else:
								link_join = urlparse.urljoin(response.url,link)
								yield scrapy.Request(url=link_join, callback=self.parse_inside)
							nitem -= 1
						else:
							break
				try:
					next_page = driver.find_element_by_xpath('//a[contains(@rel, "next")]')
					next_page.click()
				except:
					break
				time.sleep(10)
			else:
				break

		driver.close()				

	def parse_inside(self, response):        

		foryield = {}

		if self.nama_barang == True:
			nama_barang_holder = response.xpath('//h1[contains(@class,"c-product-detail__name")]/text()').extract_first()
			foryield.update({'nama_barang': nama_barang_holder})	
				
		if self.deskripsi == True:
			deskripsi_holder = ' '.join(response.xpath('//div[@class="js-collapsible-product-detail qa-pd-description u-txt--break-word"]/p/text()[normalize-space()]').extract())
			foryield.update({'deskripsi': deskripsi_holder})	

		if self.harga == True:
			harga_holder = response.xpath('//div[contains(@class,"c-product-detail-price")]/span/span[contains(@class,"amount")]/text()').extract_first()
			foryield.update({'harga': harga_holder})	

		if self.kategori == True:
			kategori_holder = response.xpath('//dd[@class="c-deflist__value qa-pd-category-value qa-pd-category"]/text()').extract_first()
			foryield.update({'kategori': kategori_holder})

		yield foryield

class TokopediaResult(scrapy.Spider):

	name = "tokopedia"

	def start_requests(self):
		urls = [
			'https://www.tokopedia.com',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		if self.keyword is not None:
			yield scrapy.Request(url=response.url, callback=self.pair_one, dont_filter=True, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
		elif self.penjual is not None:
			if self.etalase is None:
				yield scrapy.Request(url="https://www.tokopedia.com/"+self.penjual,callback=self.pair_two)
			else:
				yield scrapy.Request(url="https://www.tokopedia.com/"+self.penjual+"/etalase/"+self.etalase,callback=self.pair_two)

	def pair_one(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		text_box = driver.find_element_by_xpath('//input[@id="search-keyword"]')
		submit_button = driver.find_element_by_xpath('//button[contains(@class, "new-btn-search")]')
		text_box.send_keys(self.keyword)
		submit_button.click()
		time.sleep(3)
		try:
			popupbox_close = driver.find_element_by_xpath('//a[contains(@class,"hopscotch-bubble-close")]')
			popupbox_close.click()
			time.sleep(1)
		except Exception as e: 
			print "no pop up screen"
			time.sleep(1)
		nitem = int(float(self.item_count))
		while counter < 1:
			if nitem > 0:
				allLink = driver.find_elements_by_xpath('//div[contains(@class, "product-card")]/a')
				if allLink is not None:
					for link in allLink:
						if nitem > 0:
							url_form_link = link.get_attribute("href")
							if "tokopedia.com" in url_form_link:
								# print url_form_link
								yield scrapy.Request(url=url_form_link, callback=self.parse_pair_one, meta={'web_driver':driver,'dont_redirect':True,'handle_httpstatus_list':[302]})
							else:
								link_join = urlparse.urljoin(response.url,url_form_link)
								# print link_join
								yield scrapy.Request(url=link_join, callback=self.parse_pair_one, meta={'web_driver':driver,'dont_redirect':True,'handle_httpstatus_list':[302]})
							nitem -= 1
						else:
							break
				try:
					next_page = driver.find_element_by_xpath('//div[@class="pagination pull-right"]//ul/li[last()]/a')
				except:
					break
				next_page.click()
				time.sleep(10)
			else:
				break
		driver.close()

	def parse_pair_one(self, response):        

		foryield = {}

		if self.nama_barang == True:
			nama_barang_holder = response.xpath('//h1[contains(@class,"product-title")]/a/text()').extract_first()
			foryield.update({'nama_barang': nama_barang_holder})	
				
		if self.deskripsi == True:
			deskripsi_holder = ' '.join(response.xpath('//div[contains(@class,"product-info-holder")]/p/text()[normalize-space()]').extract())
			foryield.update({'deskripsi': deskripsi_holder})	

		if self.harga == True:
			harga_holder = response.xpath('//div[contains(@class,"product-pricetag")]/span[2]/text()').extract_first()
			foryield.update({'harga': harga_holder})
		
		if self.ulasan == True:
			forulasan = []
			counterulasan = 0
			driver = response.meta['web_driver']
			driver.get(response.url)
			ulasan_button = driver.find_element_by_xpath('//a[contains(@class,"review-container")]')
			ulasan_button.click()
			time.sleep(3)
			if self.ulasanpage is not None:
				counterulasanpage = self.ulasanpage
			else:
				counterulasanpage = 1
			while counterulasan < (int(float(counterulasanpage))):
				counterulasan += 1
				selenium_response_text = driver.page_source
				selector = Selector(text=selenium_response_text)
				all_ulasan = selector.xpath('//ul[@id="review-container"]/li[@id="talk-list-container"]//div[contains(@class,"list-box-text")]/span/text()').extract()
				if all_ulasan is not None:
					for isi_ulasan in all_ulasan:
						forulasan.append(isi_ulasan)
				try:
					next_ulasan = driver.find_element_by_xpath('//a[@id="next-page"]')
					next_ulasan.click()
					time.sleep(5)
				except:
					break
			foryield.update({'ulasan': forulasan})

		yield foryield

	def pair_two(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		time.sleep(3)
		while counter < (int(float(self.penjualpage))):
			counter += 1
			allLink = driver.find_elements_by_xpath('//div[contains(@class, "shop-product")]/a')
			if allLink is not None:
				for link in allLink:
					url_form_link = link.get_attribute("href")
					# print url_form_link
					if "tokopedia.com" in url_form_link:
						# print url_form_link
						yield scrapy.Request(url=url_form_link, callback=self.parse_pair_two, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
					else:
						link_join = urlparse.urljoin(response.url,url_form_link)
						# print link_join
						yield scrapy.Request(url=link_join, callback=self.parse_pair_one, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
			try:
				next_page = driver.find_element_by_xpath('//div[@class="pagination pull-right"]//ul/li[last()]/a')
			except:
				break
			next_page.click()
			time.sleep(5)
		driver.close()

	def parse_pair_two(self, response):        

		foryield = {}

		if self.nama_barang_p == True:
			nama_barang_holder = response.xpath('//h1[contains(@class,"product-title")]/a/text()').extract_first()
			foryield.update({'nama_barang_p': nama_barang_holder})	
				
		if self.deskripsi_p == True:
			deskripsi_holder = ' '.join(response.xpath('//div[contains(@class,"product-info-holder")]/p/text()[normalize-space()]').extract())
			foryield.update({'deskripsi_p': deskripsi_holder})	
				
		if self.harga_p == True:
			harga_holder = response.xpath('//div[contains(@class,"product-pricetag")]/span[2]/text()').extract_first()
			foryield.update({'harga_p': harga_holder})
		
		if self.ulasan_p == True:
			forulasan = []
			counterulasan = 0
			driver = webdriver.Firefox()
			driver.get(response.url)
			ulasan_button = driver.find_element_by_xpath('//a[contains(@class,"review-container")]')
			ulasan_button.click()
			time.sleep(3)
			if self.ulasanpage is not None:
				counterulasanpage = self.ulasanpage
			else:
				counterulasanpage = 1
			while counterulasan < (int(float(counterulasanpage))):
				counterulasan += 1
				selenium_response_text = driver.page_source
				selector = Selector(text=selenium_response_text)
				all_ulasan = selector.xpath('//ul[@id="review-container"]/li[@id="talk-list-container"]//div[contains(@class,"list-box-text")]/span/text()').extract()
				if all_ulasan is not None:
					for isi_ulasan in all_ulasan:
						forulasan.append(isi_ulasan)
				try:
					next_ulasan = driver.find_element_by_xpath('//a[@id="next-page"]')
					next_ulasan.click()
					time.sleep(5)
				except:
					break
			foryield.update({'ulasan_p': forulasan})
			driver.close()

		yield foryield

class DetikResult(scrapy.Spider):

	name = "detik"

	def start_requests(self):
		urls = [
			'https://www.detik.com',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		if self.keyword is not None:
			yield scrapy.Request(url=response.url, callback=self.pair_one, dont_filter=True, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
		elif self.jumlah_fokus is not None:
			yield scrapy.Request(url=response.url, callback=self.pair_two, dont_filter=True, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
	
	def pair_one(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		text_box = driver.find_element_by_xpath('//div[contains(@class,"new_search")]/form/input[contains(@class,"text")]')
		submit_button = driver.find_element_by_xpath('//div[contains(@class,"new_search")]/form/input[contains(@class,"btn_s")]')
		text_box.send_keys(self.keyword)
		submit_button.click()
		time.sleep(3)
		nitem = int(float(self.item_count))
		while counter < 1:
			if nitem > 0:
				allLink = driver.find_elements_by_xpath('//div[contains(@class,"list-berita")]/article[not(@class)]/a')
				if allLink is not None:
					for link in allLink:
						if nitem > 0:
							url_form_link = link.get_attribute("href")
							if "detik.com" in url_form_link:
								yield scrapy.Request(url=url_form_link, callback=self.parse_pair_one, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
							else:
								link_join = urlparse.urljoin(response.url,url_form_link)
								yield scrapy.Request(url=link_join, callback=self.parse_pair_one, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
							nitem -= 1
						else:
							break
				try:
					next_page = driver.find_element_by_xpath('//div[@class="paging text_center"]/a[@class="last"]/img[@alt="Kanan"]')
					driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
					time.sleep(1)
					next_page.click()
				except:
					break
				time.sleep(10)
			else:
				break
		driver.close()

	def parse_pair_one(self, response):        

		foryield = {}
		
		if self.judul == True:
			judul_holder = response.xpath('//div[contains(@class,"jdl")]/h1/text()').extract_first()
			foryield.update({'judul': judul_holder})	
				
		if self.tanggal == True:
			tanggal_holder = response.xpath('//div[contains(@class,"jdl")]/div[contains(@class,"date")]/text()').extract_first()
			foryield.update({'tanggal': tanggal_holder})	

		if self.penulis == True:
			penulis_holder = response.xpath('//div[contains(@class,"jdl")]/div[contains(@class,"author")]/text()').extract_first()
			foryield.update({'penulis': penulis_holder})

		if self.isi == True:
			isi_holder = ' '.join(response.xpath('//div[@id="detikdetailtext"]/text()[normalize-space()]').extract())
			foryield.update({'isi': isi_holder})
		
		if self.komentar == True:
			forkomentar = []
			counterkomentar = 0
			driver = webdriver.Firefox()
			driver.get(response.url)
			if self.komentar_page is not None:
				counterkomentarpage = self.komentar_page
			else:
				counterkomentarpage = 1
			while counterkomentar < (int(float(counterkomentarpage))):
				counterkomentar += 1
				try:
					frame = driver.find_element_by_xpath('//iframe[@id="forframe"]')
					time.sleep(1)
					driver.switch_to.frame(frame)
					selenium_response_text = driver.page_source
					selector = Selector(text=selenium_response_text)
					all_komentar = selector.xpath('//ul[contains(@class,"comment_list")]/li[contains(@class,"list_dk")]/div[contains(@class,"list_dk_text")]/div[contains(@class,"dk_komen")]/text()').extract()
					if all_komentar is not None:
						for isi_komentar in all_komentar:
							forkomentar.append(isi_komentar)
					try:
						next_komentar = driver.find_element_by_xpath('//div[contains(@class,"dk_page")]/a[contains(@class,"selected")]/following-sibling::a[1]')
						next_komentar.click()
						time.sleep(5)
					except:
						break
				except:
					pass
			foryield.update({'komentar': forkomentar})
			foryield.update({'url': response.url})
			driver.close()

		yield foryield

	def pair_two(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		try:
			banner_close = driver.find_element_by_xpath('//span[contains(@class,"close_otp")]')
			banner_close.click()
			time.sleep(1)
		except Exception as e: 
			print "no banner"
			time.sleep(1)
		allLink = driver.find_elements_by_xpath('//div[contains(@class,"boxlr")][position() = 1]/ul[contains(@class,"list_fokus")]/li[not(@class)]/article/a')
		if allLink is not None:
			for link in allLink:
				if (counter < (int(float(self.jumlah_fokus)))) and (counter < 10):
					url_form_link = link.get_attribute("href")
					title_fokus = link.find_element_by_xpath('./div[contains(@class,"title_lnf")]/h2/span').text
					if "detik.com" in url_form_link:
						yield scrapy.Request(url=url_form_link, callback=self.parse_pair_two, meta={'title_fokus':title_fokus,'dont_redirect':True,'handle_httpstatus_list':[302,301]})
					else:
						link_join = urlparse.urljoin(response.url,url_form_link)
						yield scrapy.Request(url=link_join, callback=self.parse_pair_two, meta={'title_fokus':title_fokus,'dont_redirect':True,'handle_httpstatus_list':[302,301]})
				else:
					break
				counter += 1
		driver.close()

	def parse_pair_two(self, response):
		driver = webdriver.Firefox()
		driver.get(response.url)
		last_height = driver.execute_script("return document.body.scrollHeight")

		while True:
		    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
		    time.sleep(2)
		    new_height = driver.execute_script("return document.body.scrollHeight")
		    if new_height == last_height:
		        break
		    last_height = new_height

		allLink = driver.find_elements_by_xpath('//div[@class="list_artikel"]/div/article/a')
		if allLink is not None:
			for link in allLink:
				url_form_link = link.get_attribute("href")
				if "detik.com" in url_form_link:
					yield scrapy.Request(url=url_form_link, callback=self.parse_inside_pair_two, meta={'title_fokus':response.meta['title_fokus'],'dont_redirect':True,'handle_httpstatus_list':[302,301]})
				else:
					link_join = urlparse.urljoin(response.url,url_form_link)
					yield scrapy.Request(url=link_join, callback=self.parse_inside_pair_two, meta={'title_fokus':response.meta['title_fokus'],'dont_redirect':True,'handle_httpstatus_list':[302,301]})
		driver.close()

	def parse_inside_pair_two(self, response):        

		foryield = {}
		
		foryield.update({'fokus_title': response.meta['title_fokus']})

		if self.judul == True:
			judul_holder = response.xpath('//div[contains(@class,"jdl")]/h1/text()').extract_first()
			foryield.update({'judul': judul_holder})	
				
		if self.tanggal == True:
			tanggal_holder = response.xpath('//div[contains(@class,"jdl")]/div[contains(@class,"date")]/text()').extract_first()
			foryield.update({'tanggal': tanggal_holder})	

		if self.penulis == True:
			penulis_holder = response.xpath('//div[contains(@class,"jdl")]/div[contains(@class,"author")]/text()').extract_first()
			foryield.update({'penulis': penulis_holder})

		if self.isi == True:
			isi_holder = ' '.join(response.xpath('//div[@id="detikdetailtext"]/text()[normalize-space()]').extract())
			foryield.update({'isi': isi_holder})
		
		if self.komentar == True:
			forkomentar = []
			counterkomentar = 0
			driver = webdriver.Firefox()
			driver.get(response.url)
			if self.komentar_page is not None:
				counterkomentarpage = self.komentar_page
			else:
				counterkomentarpage = 1
			while counterkomentar < (int(float(counterkomentarpage))):
				counterkomentar += 1
				try:
					frame = driver.find_element_by_xpath('//iframe[@id="forframe"]')
					time.sleep(1)
					driver.switch_to.frame(frame)
					selenium_response_text = driver.page_source
					selector = Selector(text=selenium_response_text)
					all_komentar = selector.xpath('//ul[contains(@class,"comment_list")]/li[contains(@class,"list_dk")]/div[contains(@class,"list_dk_text")]/div[contains(@class,"dk_komen")]/text()').extract()
					if all_komentar is not None:
						for isi_komentar in all_komentar:
							forkomentar.append(isi_komentar)
					try:
						next_komentar = driver.find_element_by_xpath('//div[contains(@class,"dk_page")]/a[contains(@class,"selected")]/following-sibling::a[1]')
						next_komentar.click()
						time.sleep(5)
					except:
						break
				except:
					pass
			foryield.update({'komentar': forkomentar})
			foryield.update({'url': response.url})
			driver.close()

		yield foryield

class KompasResult(scrapy.Spider,IDriver):

	name = "kompas"
	
	def start_requests(self):
		urls = [
			'http://www.kompas.com',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		if self.keyword is not None:
			yield scrapy.Request(url=response.url, callback=self.pair_one, dont_filter=True, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
		elif self.tag_slug is not None:
			yield scrapy.Request(url="http://indeks.kompas.com/tag/"+self.tag_slug, callback=self.pair_two, dont_filter=True, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})

	def pair_one(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		text_box = driver.find_element_by_xpath('//input[@id="search"]')
		submit_button = driver.find_element_by_xpath('//input[@name="submit"]')
		text_box.send_keys(self.keyword)
		submit_button.click()
		time.sleep(3)
		nitem = int(float(self.item_count))
		while counter < 1:
			if nitem > 0:
				allLink = driver.find_elements_by_xpath('//div[contains(@class,"gsc-thumbnail-inside")]/div[contains(@class,"gs-title")]/a[@href]')
				if allLink is not None:
					for link in allLink:
						if nitem > 0:
							url_form_link = link.get_attribute("href")
							if "kompas.com" in url_form_link:
								yield scrapy.Request(url=url_form_link, callback=self.parse_pair_one, meta={'web_driver':driver})
							else:
								link_join = urlparse.urljoin(response.url,url_form_link)
								yield scrapy.Request(url=link_join, callback=self.parse_pair_one, meta={'web_driver':driver})
							nitem -= 1
						else:
							break
				try:
					next_page = driver.find_element_by_xpath('//div[@class="gsc-cursor-page gsc-cursor-current-page"]/following-sibling::div[position()=1]')
					driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
					time.sleep(1)
					next_page.click()
				except:
					break
				time.sleep(3)
			else:
				break
		# driver.close()

	def parse_pair_one(self, response):

		foryield = {}

		if self.judul == True:
			judul_holder = response.xpath('//h1[contains(@class,"read__title")]/text()').extract_first()
			foryield.update({'judul': judul_holder})

		if self.penulis == True:
			penulis_holder = response.xpath('//div[contains(@class,"read__author")]/text()').extract_first()
			foryield.update({'penulis': penulis_holder})

		if self.tanggal == True:
			tanggal_holder = response.xpath('//div[contains(@class,"read__time")]/text()').extract_first()
			foryield.update({'tanggal': tanggal_holder})

		if self.isi == True:
			isi_holder = ' '.join(response.xpath('//div[contains(@class,"read__content")]//text()[normalize-space()]').extract())
			foryield.update({'isi': isi_holder})

		if self.komentar == True:
			forkomentar = []
			counterkomentar = 0
			driver = response.meta['web_driver']
			driver.get(response.url)
			if self.komentar_page is not None:
				counterkomentarpage = self.komentar_page
			else:
				counterkomentarpage = 1
			while counterkomentar < (int(float(counterkomentarpage))):
				try:
					next_komentar = driver.find_element_by_xpath('//div[@id="load-more"]/a')
					element_scroll = driver.find_element_by_xpath('//div[@class="row mt2 clearfix"][last()]/h2')
					driver.execute_script("arguments[0].scrollIntoView(false);", element_scroll)
					next_komentar.click()
					time.sleep(5)
				except:
					pass
				counterkomentar += 1
			try:
				selenium_response_text = driver.page_source
				selector = Selector(text=selenium_response_text)
				all_komentar = selector.xpath('//div[@id="komentar"]//div[@id="comment-box"]/div[@class="item"]/p[@class="message"]/text()[normalize-space()]').extract()
				if all_komentar is not None:
					for isi_komentar in all_komentar:
						forkomentar.append(isi_komentar)
			except:
				pass
			foryield.update({'komentar': forkomentar})

		foryield.update({'url': response.url})

		yield foryield

	def pair_two(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		time.sleep(5)
		while counter < (int(float(self.page_count_tag))):
			counter += 1
			allLink = driver.find_elements_by_xpath('//div[@class="article__list clearfix"]/div[@class="article__list__title"]/h3/a[@href]')
			if allLink is not None:
				for link in allLink:
					url_form_link = link.get_attribute("href")
					if "kompas.com" in url_form_link:
						yield scrapy.Request(url=url_form_link, callback=self.parse_pair_two, meta={'web_driver':driver})
					else:
						link_join = urlparse.urljoin(response.url,url_form_link)
						yield scrapy.Request(url=link_join, callback=self.parse_pair_two, meta={'web_driver':driver})
			try:
				next_page = driver.find_element_by_xpath('//div[@class="paging__item"]/a[contains(@class,"paging__link--next")]')
				driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
				time.sleep(1)
				next_page.click()
			except:
				break
			time.sleep(10)
		driver.close()

	def parse_pair_two(self, response):
		
		foryield = {}

		if self.judul == True:
			judul_holder = response.xpath('//h1[contains(@class,"read__title")]/text()').extract_first()
			foryield.update({'judul': judul_holder})

		if self.penulis == True:
			penulis_holder = response.xpath('//div[contains(@class,"read__author")]/text()').extract_first()
			foryield.update({'penulis': penulis_holder})

		if self.tanggal == True:
			tanggal_holder = response.xpath('//div[contains(@class,"read__time")]/text()').extract_first()
			foryield.update({'tanggal': tanggal_holder})

		if self.isi == True:
			isi_holder = ' '.join(response.xpath('//div[contains(@class,"read__content")]//text()[normalize-space()]').extract())
			foryield.update({'isi': isi_holder})

		if self.komentar == True:
			forkomentar = []
			counterkomentar = 0
			driver = response.meta['web_driver']
			driver.get(response.url)
			if self.komentar_page is not None:
				counterkomentarpage = self.komentar_page
			else:
				counterkomentarpage = 1
			while counterkomentar < (int(float(counterkomentarpage))):
				try:
					next_komentar = driver.find_element_by_xpath('//div[@id="load-more"]/a')
					element_scroll = driver.find_element_by_xpath('//div[@class="row mt2 clearfix"][last()]/h2')
					driver.execute_script("arguments[0].scrollIntoView(false);", element_scroll)
					next_komentar.click()
					time.sleep(5)
				except:
					pass
				counterkomentar += 1
			try:
				selenium_response_text = driver.page_source
				selector = Selector(text=selenium_response_text)
				all_komentar = selector.xpath('//div[@id="komentar"]//div[@id="comment-box"]/div[@class="item"]/p[@class="message"]/text()[normalize-space()]').extract()
				if all_komentar is not None:
					for isi_komentar in all_komentar:
						forkomentar.append(isi_komentar)
			except:
				pass
			foryield.update({'komentar': forkomentar})

		if self.tag == True:
			fortag = []
			all_tag = response.xpath('//ul[@class="tag__article__wrap"]/li[@class="tag__article__item"]/a/text()').extract()
			if all_tag is not None:
				for tag in all_tag:
						fortag.append(tag)

		foryield.update({'tag': fortag})
		foryield.update({'url': response.url})

		yield foryield

class BugzillaResult(scrapy.Spider,IDriver):

	name = "bugzilla"

	def start_requests(self):
		urls = [
			'http://www.bugzilla.org',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.first_call)
			request.meta['counter'] = 0
			yield request

	def first_call(self, response):
		# print "wata"
		yield scrapy.Request(url='https://bugzilla.'+self.bzurl+'.org/index.cgi', callback=self.parse)

	def parse(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		text_box = driver.find_element_by_xpath('//input[@id="quicksearch_main"]')
		submit_button = driver.find_element_by_xpath('//input[@id="find"]')
		text_box.send_keys(self.keyword)
		submit_button.click()
		time.sleep(5)
		try:
			show_all = driver.find_element_by_xpath('//span[@class="bz_result_count"][position()=1]/a')
			show_all.click()
		except:
			pass
		selenium_response_text = driver.page_source
		selector = Selector(text=selenium_response_text)
		allLink = selector.xpath('//td[contains(@class, "bz_id_column")]/a/@href').extract()
		if allLink is not None:
			for link in allLink:
				if counter < (int(float(self.item_count))):
					if "http://bugzilla" in link:
						yield scrapy.Request(url=link, callback=self.parse_inside)
					else:
						link_join = urlparse.urljoin(response.url,link)
						yield scrapy.Request(url=link_join, callback=self.parse_inside)
				counter += 1

		time.sleep(5)

		driver.close()				

	def parse_inside(self, response):        

		foryield = {}

		if self.id == True:
			id_holder = response.xpath('//div[@class="bz_short_desc_container edit_form"]/a/b/text()').extract_first()
			foryield.update({'id': id_holder})	
				
		if self.nama_bug == True:
			nama_bug_holder = response.xpath('//span[@id="short_desc_nonedit_display"]/text()').extract_first()
			foryield.update({'nama_bug': nama_bug_holder})	

		if self.description == True:
			description_holder = ' '.join(response.xpath('//div[@id="c0"]//pre[@class="bz_comment_text"]//text()').extract())
			foryield.update({'description': description_holder})	

		if self.comment == True:
			forkomentar = []
			all_comment = response.xpath('//div[@class="bz_comment"]//pre[@class="bz_comment_text"]//text()').extract()
			if all_comment is not None:
				for isi_komentar in all_comment:
					forkomentar.append(isi_komentar)
			foryield.update({'komentar': forkomentar})

		if self.status == True:
			status_holder = response.xpath('//span[@id="static_bug_status"]/text()').extract_first()
			foryield.update({'status': status_holder})	

		if self.reported == True:
			reported_holder = response.xpath('//tr[@id="field_tablerow_reported"]/td/text()').extract_first()
			foryield.update({'reported': reported_holder})

		if self.modified == True:
			modified_holder = response.xpath('//tr[@id="field_tablerow_modified"]/td/text()').extract_first()
			foryield.update({'modified': modified_holder})	

		yield foryield

class LpseResult(scrapy.Spider):

	name = "lpse"
	
	def start_requests(self):
		urls = [
			'https://inaproc.lkpp.go.id/v3/daftar_lpse',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse, meta={'dont_redirect':True,'handle_httpstatus_list':[302]})
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		xpath_string = '//table//tr/td[text()="'+self.id_lpse+'"]/following-sibling::td[position()=1]/a/@href'
		lpse_url = response.xpath(xpath_string).extract_first()
		yield scrapy.Request(url=lpse_url+'/eproc/lelang', callback=self.pair_one, dont_filter=True, meta={'dont_redirect':True,'handle_httpstatus_list':[302,301]})

	def pair_one(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		text_box = driver.find_element_by_xpath('//input[@id="textfield"]')
		submit_button = driver.find_element_by_xpath('//form[@id="search"]/input[position()=2]')
		text_box.send_keys(self.keyword)
		submit_button.click()
		time.sleep(3)
		nitem = int(float(self.item_count))
		while counter < 1:
			if nitem > 0:
				allLink = driver.find_elements_by_xpath('//table[@class="t-data-grid"]//tr/td[@class="pkt_nama"]/b/a[@href]')
				if allLink is not None:
					for link in allLink:
						if nitem > 0:
							print nitem
							url_form_link = link.get_attribute("href")
							if "https://lpse." in url_form_link:
								yield scrapy.Request(url=url_form_link, callback=self.parse_pair_one, meta={'web_driver':driver}, dont_filter=True)
							else:
								link_join = urlparse.urljoin(response.url,url_form_link)
								yield scrapy.Request(url=link_join, callback=self.parse_pair_one, meta={'web_driver':driver}, dont_filter=True)
							nitem -= 1
						else:
							break
				try:
					next_page = driver.find_element_by_xpath('//div[@class="t-data-grid-pager"]/span[@class="current"]/following-sibling::a[position()=1]')
					driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
					time.sleep(1)
					next_page.click()
				except:
					break
				time.sleep(5)
			else:
				break
		driver.close()

	def parse_pair_one(self, response):

		foryield = {}

		if self.kode_lelang == True:
			kode_lelang_holder = response.xpath('//table/tr/td[text()="Kode Lelang"]/following-sibling::td//text()').extract_first()
			foryield.update({'kode_lelang': kode_lelang_holder})

		if self.nama_lelang == True:
			nama_lelang_holder = response.xpath('//table/tr/td[text()="Nama Lelang " or text()="Nama Lelang (Lelang Ulang)"]/following-sibling::td//text()').extract_first()
			foryield.update({'nama_lelang': nama_lelang_holder})

		if self.keterangan == True:
			keterangan_holder = response.xpath('//table/tr/td[text()="Keterangan"]/following-sibling::td//text()').extract_first()
			foryield.update({'keterangan': keterangan_holder})

		if self.instansi == True:
			instansi_holder = response.xpath('//table/tr/td[text()="Instansi"]/following-sibling::td//text()').extract_first()
			foryield.update({'instansi': instansi_holder})

		if self.satuan_kerja == True:
			satuan_kerja_holder = response.xpath('//table/tr/td[text()="Satuan Kerja"]/following-sibling::td//text()').extract_first()
			foryield.update({'satuan_kerja': satuan_kerja_holder})

		if self.kategori == True:
			kategori_holder = response.xpath('//table/tr/td[text()="Kategori"]/following-sibling::td//text()').extract_first()
			foryield.update({'kategori': kategori_holder})

		if self.metode_pengadaan == True:
			metode_pengadaan_holder = response.xpath('//table/tr/td[text()="Metode Pengadaan"]/following-sibling::td[1]//text()').extract_first()
			foryield.update({'metode_pengadaan': metode_pengadaan_holder})

		if self.metode_kualifikasi == True:
			metode_kualifikasi_holder = response.xpath('//table/tr/td[text()="Metode Pengadaan"]/following-sibling::td[3]//text()').extract_first()
			foryield.update({'metode_kualifikasi': metode_kualifikasi_holder})

		if self.metode_dokumen == True:
			metode_dokumen_holder = response.xpath('//table/tr/td[text()="Metode Dokumen"]/following-sibling::td[1]//text()').extract_first()
			foryield.update({'metode_dokumen': metode_dokumen_holder})

		if self.metode_evaluasi == True:
			metode_evaluasi_holder = response.xpath('//table/tr/td[text()="Metode Dokumen"]/following-sibling::td[3]//text()').extract_first()
			foryield.update({'metode_evaluasi': metode_evaluasi_holder})

		if self.anggaran == True:
			anggaran_holder = response.xpath('//table/tr/td[text()="Anggaran"]/following-sibling::td//text()').extract_first()
			foryield.update({'anggaran': anggaran_holder})

		if self.nilai_pagu_paket == True:
			nilai_pagu_paket_holder = response.xpath('//table/tr/td[text()="Nilai Pagu Paket"]/following-sibling::td[1]//text()').extract_first()
			foryield.update({'nilai_pagu_paket': nilai_pagu_paket_holder})

		if self.nilai_hps_paket == True:
			nilai_hps_paket_holder = response.xpath('//table/tr/td[text()="Nilai Pagu Paket"]/following-sibling::td[3]//text()').extract_first()
			foryield.update({'nilai_hps_paket': nilai_hps_paket_holder})

		if self.cara_pembayaran == True:
			cara_pembayaran_holder = response.xpath('//table/tr/td[text()="Cara Pembayaran"]/following-sibling::td//text()').extract_first()
			foryield.update({'cara_pembayaran': cara_pembayaran_holder})

		if self.pembebanan_tahun_anggaran == True:
			pembebanan_tahun_anggaran_holder = response.xpath('//table/tr/td[text()="Pembebanan Tahun Anggaran"]/following-sibling::td//text()').extract_first()
			foryield.update({'pembebanan_tahun_anggaran': pembebanan_tahun_anggaran_holder})

		if self.sumber_dana == True:
			sumber_dana_holder = response.xpath('//table/tr/td[text()="Sumber Dana"]/following-sibling::td//text()').extract_first()
			foryield.update({'sumber_dana': sumber_dana_holder})

		if self.bobot_teknis == True:
			bobot_teknis_holder = response.xpath('//table/tr/td[text()="Bobot Teknis"]/following-sibling::td[1]//text()').extract_first()
			foryield.update({'bobot_teknis': bobot_teknis_holder})

		if self.bobot_biaya == True:
			bobot_biaya_holder = response.xpath('//table/tr/td[text()="Bobot Teknis"]/following-sibling::td[3]//text()').extract_first()
			foryield.update({'bobot_biaya': bobot_biaya_holder})

		if self.lokasi_pekerjaan == True:
			lokasi_pekerjaan_holder = response.xpath('//table/tr/td[text()="Lokasi Pekerjaan"]/following-sibling::td//text()').extract_first()
			foryield.update({'lokasi_pekerjaan': lokasi_pekerjaan_holder})

		if self.syarat_kualifikasi == True:
			forsyarat = []
			syarat_kualifikasi_list = response.xpath('//table/tr/td[text()="Syarat Kualifikasi"]/following-sibling::td/table/tr')
			for syt_klf in syarat_kualifikasi_list:
				if syt_klf.xpath('./td[2]/table'):
					ijin_usaha_list = syt_klf.xpath('./td[2]/table/tr')
					for ijin_usaha in ijin_usaha_list:
						forijin = []
						ijin1 = ijin_usaha.xpath('./td[1]/text()').extract_first()
						ijin2 = ijin_usaha.xpath('./td[2]/text()').extract_first()
						forijin.append(ijin1)
						forijin.append(ijin2)
						forsyarat.append(forijin)
				else:
					syarat = ', '.join(syt_klf.xpath('./td[2]/text()').extract())
					forsyarat.append(syarat)
			foryield.update({'syarat_kualifikasi': forsyarat})

		yield foryield

class ShopeePopuler(scrapy.Spider):

	name = "shopeepopuler"

	def start_requests(self):
		urls = [
			'https://shopee.co.id/popular_products/1/',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse)
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		time.sleep(2)
		foryield = {}

		while counter < 34:
			counter += 1
			selenium_response_text = driver.page_source
			selector = Selector(text=selenium_response_text)
			item_list = selector.xpath('//div[@class="shopee-item-card__lower-padding"]')
			for item in item_list:
				nama_barang_holder = item.xpath('./div[@class="shopee-item-card__text-name"]/text()').extract_first()
				harga_holder = item.xpath('./div[@class="shopee-item-card__section-price"]/div[@class="shopee-item-card__current-price shopee-item-card__current-price--free-shipping" or @class="shopee-item-card__current-price"]/text()').extract_first()
				disukai_holder = item.xpath('.//div[@class="shopee-item-card__btn-like__text"]/text()').extract_first()
				reviewer_holder = item.xpath('./div[@class="shopee-item-card__section-actions"]/div[@class="shopee-item-card__btn-comments"]/span/text()').extract_first()
				foryield.update({'nama_barang': nama_barang_holder})
				foryield.update({'harga': harga_holder})
				foryield.update({'disukai': disukai_holder})
				foryield.update({'reviewer': reviewer_holder})
				yield foryield
			try:
				next_page = driver.find_element_by_xpath('//button[contains(@class, "shopee-icon-button--right")]')
				driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
				time.sleep(1)
				next_page.click()
			except:
				break
			time.sleep(5)
		driver.close()

class ShopeeKategori(scrapy.Spider):

	name = "shopeekategori"

	def start_requests(self):
		urls = [
			'https://shopee.co.id',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse)
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		driver = webdriver.Chrome()
		driver.get(response.url)
		time.sleep(2)
		selenium_response_text = driver.page_source
		selector = Selector(text=selenium_response_text)
		allLink = selector.xpath('//a[@class="home-category-list__category-grid"]')
		if allLink is not None:
			for link in allLink:
				url_form_link = link.xpath("./@href").extract_first()
				kategori = link.xpath(".//text()").extract_first()
				link_join = urlparse.urljoin('https://shopee.co.id',url_form_link)
				# print link_join
				yield scrapy.Request(url=link_join, callback=self.parse_inside, meta={'kategori':kategori})
		driver.close()
	
	def parse_inside(self, response):
		foryield = {}
		driver = webdriver.Firefox()
		driver.get(response.url+"?sortBy=sales")
		# try:
		# 	time.sleep(3)
		# 	terpopuler_button = driver.find_element_by_xpath('//div[@class="shopee-sort-by-options"]/a[3]')
		# 	terpopuler_button.click()
		# 	time.sleep(3)
		# except:
		# 	pass
		counter = 0
		while counter < 2:
			counter += 1
			selenium_response_text = driver.page_source
			selector = Selector(text=selenium_response_text)
			item_list = selector.xpath('//div[@class="shopee-item-card__lower-padding"]')
			for item in item_list:
				nama_barang_holder = item.xpath('./div[@class="shopee-item-card__text-name"]/text()').extract_first()
				harga_holder = item.xpath('./div[@class="shopee-item-card__section-price"]/div[@class="shopee-item-card__current-price shopee-item-card__current-price--free-shipping" or @class="shopee-item-card__current-price"]/text()').extract_first()
				jumlah_terjual_holder = item.xpath('.//div[@class="shopee-item-card__label-sold-count"]/text()').extract_first()
				reviewer_holder = item.xpath('./div[@class="shopee-item-card__section-actions"]/div[@class="shopee-item-card__btn-comments"]/span/text()').extract_first()
				kategori_holder = response.meta['kategori']
				foryield.update({'harga': harga_holder})
				foryield.update({'jumlah_terjual': jumlah_terjual_holder})
				foryield.update({'reviewer': reviewer_holder})
				foryield.update({'kategori': kategori_holder})
				foryield.update({'nama_barang': nama_barang_holder})
				yield foryield
			try:
				next_page = driver.find_element_by_xpath('//button[contains(@class, "shopee-icon-button--right")]')
				driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
				time.sleep(1)
				next_page.click()
			except:
				break
			time.sleep(5)
		driver.close()

class Kpu(scrapy.Spider):

	name = "kpu"

	def start_requests(self):
		urls = [
			# 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/BANTEN/KOTA%20TANGERANG/CIBODAS/CIBODASARI/10',
			# 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/KALIMANTAN%20UTARA/KOTA%20TARAKAN/TARAKAN%20UTARA/JUATA%20KERIKIL',
			'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/KALIMANTAN%20UTARA/KOTA%20TARAKAN/TARAKAN%20UTARA',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse)
			request.meta['counter'] = 0
			yield request

	def parse(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		while counter < 1:
			allLink = driver.find_elements_by_xpath("//table[@id='listKecamatan']/tbody/tr/td[2]/a[@href]")
			if allLink is not None:
				for link in allLink:
					url_form_link = link.get_attribute("href")
					nama_kelurahan = link.get_attribute("text")
					link_join = urlparse.urljoin("https://infopemilu.kpu.go.id",url_form_link)
					yield scrapy.Request(url=link_join, callback=self.parse_inside_two, meta={'kelurahan':nama_kelurahan})
			try:
				next_page = driver.find_element_by_xpath("//a[@id='listKecamatan_next' and @class='paginate_button next']")
				driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
				time.sleep(1)
				next_page.click()
			except:
				break
			time.sleep(5)
		driver.close()

	def parse_inside_two(self, response):
		counter = 0
		driver = webdriver.Firefox()
		driver.get(response.url)
		while counter < 1:
			allLink = driver.find_elements_by_xpath("//table[@id='listKelurahan']/tbody/tr/td[2]/a[@href]")
			if allLink is not None:
				for link in allLink:
					url_form_link = link.get_attribute("href")
					nama_tps = link.get_attribute("text")
					link_join = urlparse.urljoin("https://infopemilu.kpu.go.id",url_form_link)
					yield scrapy.Request(url=link_join, callback=self.parse_inside_one, meta={'kelurahan':response.meta['kelurahan']})
			try:
				next_page = driver.find_element_by_xpath("//a[@id='listKelurahan_next' and @class='paginate_button next']")
				driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
				time.sleep(1)
				next_page.click()
			except:
				break
			time.sleep(5)
		driver.close()

	def parse_inside_one(self, response):

		counter = 0
		# driver = response.meta['web_driver']
		driver = webdriver.Firefox()
		driver.get(response.url)
		foryield = {}

		while counter < 1:

			selenium_response_text = driver.page_source
			selector = Selector(text=selenium_response_text)
			item_list = selector.xpath("//table[@id='listDps']/tbody/tr")
			for item in item_list:
				nomor_holder = item.xpath('./td[1]/text()').extract_first()
				nik_holder = item.xpath('./td[2]/text()').extract_first()
				nama_holder = item.xpath('./td[3]/text()').extract_first()
				tempat_lahir_holder = item.xpath('./td[4]/text()').extract_first()
				jenis_kelamin_holder = item.xpath('./td[5]/text()').extract_first()
				tps_holder = item.xpath('./td[6]/text()').extract_first()
				foryield.update({'nomor': nomor_holder})
				foryield.update({'NIK': nik_holder})
				foryield.update({'Nama': nama_holder})
				foryield.update({'Tempat_Lahir': tempat_lahir_holder})
				foryield.update({'Jenis_Kelamin': jenis_kelamin_holder})
				foryield.update({'TPS': tps_holder})
				foryield.update({'Kelurahan': response.meta['kelurahan']})
				yield foryield
			try:
				next_page = driver.find_element_by_xpath("//a[@id='listDps_next' and @class='paginate_button next']")
				driver.execute_script("arguments[0].scrollIntoView(false);", next_page)
				time.sleep(1)
				next_page.click()
			except:
				break
			time.sleep(5)
		driver.close()