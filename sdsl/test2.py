dic = {
	"web": "https//:www.detik.com",
	"driver": "driver.DetikResult",
	"driver_class": "DetikResult",
	"syntax": {
		"GET":{
			"judul": False,
			"tanggal": False,
			"penulis": False,
			"isi": False,
			"komentar": False,
		},
		"INKEYWORD": "keyword",
		"NPAGE": "page_count",
		"KOMENTARNPAGE": "komentar_page",
		"GETBYFOKUS":{
			"judul": False,
			"tanggal": False,
			"penulis": False,
			"isi": False,
			"komentar": False,
		},
		"NFOKUS": "jumlah_fokus",
	},
	"syntax_pair": [["GETBYFOKUS","NFOKUS"],["GET","INKEYWORD","NPAGE"]],
	"optional_syntax": {
		0:["KOMENTARNPAGE"],
		1:["KOMENTARNPAGE"],
	},
}

# validasi driver rule key
list_driver_rule_key = []
list_driver_rule_key_compare = ["web","driver","driver_class","syntax","syntax_pair","optional_syntax"]
for key_driver_rule_key,value_driver_rule_key in dic.items():
	list_driver_rule_key.append(key_driver_rule_key)
list_driver_rule_key = list(set(list_driver_rule_key))

print list_driver_rule_key
print list_driver_rule_key_compare

if len(list_driver_rule_key) == len(list_driver_rule_key_compare):
	compare_driver_rule_key = [c for c in list_driver_rule_key if c not in list_driver_rule_key_compare]
else:
	if len(list_driver_rule_key) > len(list_driver_rule_key_compare):
		compare_driver_rule_key = [c for c in list_driver_rule_key if c not in list_driver_rule_key_compare]
	else:
		compare_driver_rule_key = [c for c in list_driver_rule_key_compare if c not in list_driver_rule_key]

if len(compare_driver_rule_key) == 0:
	print "valid"
else:
	print "tidak valid"

# validasi syntax_pair & optional_syntax to syntax
list_syntax = []
for key_syntax,value_syntax in dic['syntax'].items():
	list_syntax.append(key_syntax)
list_syntax = list(set(list_syntax))

list_syntax_compare = []
for value_syntax_pair in dic['syntax_pair']:
	for value_syntax_compare in value_syntax_pair:
		list_syntax_compare.append(value_syntax_compare)

for key_syntax_opt,value_syntax_opt in dic['optional_syntax'].items():
	for value_opt in value_syntax_opt:
		list_syntax_compare.append(value_opt)
list_syntax_compare = list(set(list_syntax_compare))

print list_syntax
print list_syntax_compare

if len(list_syntax) == len(list_syntax_compare):
	compare_syntax = [c for c in list_syntax if c not in list_syntax_compare]
else:
	if len(list_syntax) > len(list_syntax_compare):
		compare_syntax = [c for c in list_syntax if c not in list_syntax_compare]
	else:
		compare_syntax = [c for c in list_syntax_compare if c not in list_syntax]

if len(compare_syntax) == 0:
	print "valid"
else:
	print "tidak valid"