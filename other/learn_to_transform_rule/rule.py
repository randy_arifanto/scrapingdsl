from lxml import etree
from pyparsing import *

rule = {
	"google":{
		"syntax":{
		"GET": {
			"gr_title":False,
			"gr_description":False,
			"gr_link":False,
			"web_title":False
		},
		"INKEYWORD":"keyword",
		"NPAGE":"page"
		}
	}	
}
# print type(rule['google']['syntax']['GET'])
item = {}
for key,values in rule['google']['syntax'].items():
	if isinstance(values,dict):
		for keychild,valueschild in values.items():
			item.update({keychild:valueschild})
	else:
		item.update({values:None})
newrule = {
	"title":True
}
# print item
# item.update(newrule)
# print item

colon           = Suppress(":")
coma            = Suppress(",")
openbracket     = Suppress("(")
closebracket    = Suppress(")")
fromquery		= Suppress("FROM")
content 	    = Word(alphas+"_", alphanums+"_")
syntax 	    	= Word(alphas).setResultsName("syntax")
driver 	    	= Word(alphas).setResultsName("driver")
category    	= Word(alphas).setResultsName("category") + colon
stringvar       = quotedString.addParseAction(removeQuotes).setResultsName("var")
booleanvar      = Group(openbracket + ZeroOrMore(Optional(coma) + content) + closebracket).setResultsName("var")
syntaxvar       = ZeroOrMore(syntax + (stringvar | booleanvar))
dsl				= category + fromquery + driver + syntaxvar

# string = '''GoogleResult: FROM google GET (link,description,title) INKEYWORD "one piece mal" NPAGE "1"'''
string = '''GoogleResult: FROM google GET (gr_title,gr_description,gr_link,web_title) INKEYWORD "site:google" NPAGE "1"'''

parsed = dsl.parseString(string)
# print vars(parsed)
# print parsed._ParseResults__tokdict['var']
# for x in parsed._ParseResults__tokdict['var']:
	# print type(x)
xml = parsed.asXML()
print xml
# print parsed.asDict()
listvar = []
doc = etree.fromstring(xml)
btags = doc.xpath('//var')
for b in btags:
	# print b.text
	if b.xpath("./ITEM"):
		templistvar = []
		for c in b.xpath("./ITEM"):
			templistvar.append(c.text)
		listvar.append(templistvar)
		# print listvar
	else:
		listvar.append(b.text)
			# print b.text
syntaxvar = []
syntax = doc.xpath('//syntax')
for syntaxitem in syntax:
	# print syntaxitem.text
	syntaxvar.append(syntaxitem.text)
# print listvar
newdict = dict(zip(syntaxvar,listvar))
# print newdict
item_from_query = {}
for key,values in newdict.items():
	if isinstance(values,list):
		for valueschild in values:
			item_from_query.update({valueschild:True})
	else:
		# print key
		item_from_query.update({rule['google']['syntax'][key]:values})
print item
print item_from_query
item.update(item_from_query)
print item