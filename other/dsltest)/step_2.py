from pyparsing import *
ParserElement.setDefaultWhitespaceChars(' \t')
# # define grammar
# greet = Word( alphas ) + "," + Word( alphas ) + "!"

# # input string
# hello = "Hello, World!"

# # parse input string
# print hello, "->", greet.parseString( hello )
def get_field_html(field):
    properties = field[2]
    label = properties["label"] if "label" in properties else field.field_name
    label_html = "<label>" + label + "</label>"
    attributes = {"name":field.field_name}
    attributes.update(properties)
    if field.field_type == "CharField" or field.field_type == "EmailField":
        attributes["type"] = "text"
    else:
        attributes["type"] = "password"
    if "label" in attributes:
        del attributes["label"]
    attributes_html = " ".join([name+"='"+value+"'" for name,value in attributes.items()])
    field_html = "<input " + attributes_html + "/>"
    return label_html + field_html + "<br/>"

def render(form):
    fields_html = "".join([get_field_html(field) for field in form.fields])
    return "<form id='" + form.form_name.lower() +"'>" + fields_html + "</form>"

input_form = '''UserForm
name:CharField -> label:Username size:25
email:EmailField -> size:25
password:PasswordField -> size:30
'''

newline = Suppress("\n")
colon = Suppress(":")
arrow = Suppress("->")
word = Word(alphas)
key = word
value = Word(alphanums)
field_type = oneOf("CharField EmailField PasswordField").setResultsName("field_type")
field_name = word.setResultsName("field_name")
form_name = word.setResultsName("form_name")
field_property = Group(key + colon + value).setResultsName("field_propertys*")
field = Group(field_name + colon + field_type + Optional(arrow + OneOrMore(field_property)) + newline).setResultsName("fields*")
form = form_name + newline + OneOrMore(field)

parsed_form = render(form.parseString(input_form))
parsed_form_old = form.parseString(input_form)
fieldtest = parsed_form_old
print parsed_form