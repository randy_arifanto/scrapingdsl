def is_ip_address(ip_address):
	components = ip_address.split(".")
	if len(components) != 4: return False
	try:
		int_components = [int(component) for component in components]
	except ValueError:
		return False
	for component in int_components:
		if component < 0 or component > 255:
			return False
	return True

cacing_string = '256.168.1.1'
cacing = is_ip_address(cacing_string)
print cacing