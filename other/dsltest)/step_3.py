from pyparsing import *

dsl = '''GoogleResult: GET GR(link,textlink,desc) WEB(title) INKEYWORD "site:itb.ac.id" NPAGE 2'''

colon           = Suppress(":")
getquery        = Suppress("GET")
coma            = Suppress(",")
openbracket     = Suppress("(")
closebracket    = Suppress(")")
inkeywordquery  = Suppress("INKEYWORD")
npagequery      = Suppress("NPAGE")
pagecount       = Word(alphanums).setResultsName("page_count")
scraper_type    = Word(alphas).setResultsName("scraper_type") + colon
content_type    = Word(alphas)
keyword         = quotedString.addParseAction(removeQuotes).setResultsName("keyword")
grcontent       = Group(Suppress("GR") + openbracket + OneOrMore(Optional(coma) + content_type) + closebracket).setResultsName("grcontent")
webcontent      = Group(Suppress("WEB") + openbracket + OneOrMore(Optional(coma) + content_type) + closebracket).setResultsName("webcontent")
stringdsl       = scraper_type + getquery + grcontent + webcontent + inkeywordquery + keyword + npagequery + pagecount

topstringdsl = scraper_type

parsed = stringdsl.parseString(dsl)
print parsed