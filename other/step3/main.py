from interface import Interface
from strategysatu import Strategysatu
from strategydua import Strategydua
from strategytiga import Strategytiga
base = Interface()
strategy1 = Strategysatu()
strategy2 = Strategydua()
strategy3 = Strategytiga()

try:
	#this is going to blow up!
	print base.find('chickens')

except NotImplementedError as e:
	print "The following exception was expected:"
	print e

print strategy1.find('satu')
print strategy2.find('dua')  
print strategy3.find('tiga')  