from scrapyspalsh.spiders import selenium_test
import os

import scrapy
from scrapy.crawler import CrawlerProcess


process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    'DOWNLOAD_DELAY' : 3,
	'FEED_FORMAT': 'json',
	'FEED_URI': 'hasil.json'
})

process.crawl(selenium_test.GoogleResult())
process.start()
