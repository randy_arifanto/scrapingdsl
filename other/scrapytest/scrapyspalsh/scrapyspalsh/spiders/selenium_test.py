import scrapy
from selenium import webdriver

class GoogleResult(scrapy.Spider):
	name = "itb"

	def start_requests(self):
		urls = [
			'https://lpse.pu.go.id/eproc/lelang/view/33640064',
		]
		for url in urls:
			request = scrapy.Request(url=url, callback=self.parse)
			yield request

	def parse(self, response):
		syarat_kualifikasi_list = response.xpath('//table/tr/td[text()="Syarat Kualifikasi"]/following-sibling::td/table/tr')
		for syt_klf in syarat_kualifikasi_list:
			if syt_klf.xpath('./td[2]/table'):
				ijin_usaha_list = syt_klf.xpath('./td[2]/table/tr')
				for ijin_usaha in ijin_usaha_list:
					ijin1 = ijin_usaha.xpath('./td[1]/text()').extract_first()
					ijin2 = ijin_usaha.xpath('./td[2]/text()').extract_first()
					print(ijin1)
					print(ijin2)
			else:
				syarat = ', '.join(syt_klf.xpath('./td[2]/text()').extract())
				print(syarat)

		# yield kode_lelang_holder