import time
start = time.time()

from running_test.running_test.spiders.running_test import RunningTest
import scrapy
from scrapy.crawler import CrawlerProcess

try:
	open('result_test/result_kompas.json', 'w').close()
except:
	pass

process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
    'DOWNLOAD_DELAY' : 3,
	'FEED_FORMAT': 'json',
	'FEED_URI': 'result_test/result_kompas.json',
})
# lpse
# kwargs = {'nilai_hps_paket': True, 'anggaran': False, 'metode_kualifikasi': False, 'instansi': True, 'metode_dokumen': False, 'bobot_biaya': False, 'kategori': True, 'kode_lelang': True, 'syarat_kualifikasi': False, 'pembebanan_tahun_anggaran': False, 'nilai_pagu_paket': True, 'cara_pembayaran': False, 'lokasi_pekerjaan': False, 'metode_pengadaan': False, 'nama_lelang': True, 'keyword': 'jalan', 'id_lpse': '627', 'metode_evaluasi': False, 'item_count': '10', 'sumber_dana': False, 'keterangan': True, 'satuan_kerja': False, 'bobot_teknis': False}
# tokopedia keyword
# kwargs = {'harga': True, 'ulasanpage': None, 'item_count': '10', 'keyword': 'kaset pita', 'etalase': None, 'deskripsi': True, 'nama_barang_p': False, 'nama_barang': True, 'harga_p': False, 'deskripsi_p': False, 'ulasan_p': False, 'ulasan': False, 'penjual': None, 'penjualpage': None}
# tokopedia penjual
# kwargs = {'harga': False, 'ulasanpage': '1', 'page_count': None, 'keyword': None, 'etalase': None, 'deskripsi': False, 'nama_barang_p': True, 'nama_barang': False, 'harga_p': True, 'deskripsi_p': True, 'ulasan_p': True, 'ulasan': False, 'penjual': 'jopperside', 'penjualpage': '1'}
# bukalapak keyword
# kwargs = {'harga': True, 'deskripsi': True, 'keyword': 'kaset pita', 'nama_barang': True, 'item_count': '3', 'kategori': True}
# detik keyword
# kwargs = {'jumlah_fokus': None, 'judul': True, 'item_count': '10', 'keyword': 'pemilu', 'penulis': True, 'isi': True, 'tanggal': True, 'komentar': False, 'komentar_page': '1'}
# detik fokus
# kwargs = {'jumlah_fokus': '1', 'judul': True, 'page_count': None, 'keyword': None, 'penulis': True, 'isi': True, 'tanggal': True, 'komentar': False, 'komentar_page': None}
# kompas keyword
kwargs = {'keyword': 'pemilu', 'judul': True, 'tag_slug': None, 'item_count': '3', 'tag': False, 'penulis': True, 'isi': True, 'tanggal': True, 'komentar': False, 'page_count_tag': None, 'komentar_page': None}
# kompas tag
# kwargs = {'keyword': None, 'judul': True, 'tag_slug': 'karyawan', 'page_count': None, 'tag': True, 'penulis': True, 'isi': True, 'tanggal': True, 'komentar': False, 'page_count_tag': '1', 'komentar_page': None}
# bugzilla
# kwargs = {'comment': False, 'status': True, 'description': True, 'keyword': 'bug', 'max_comment': None, 'nama_bug': True, 'modified': True, 'item_count': '5', 'reported': True, 'bzurl': 'kernel', 'id': True}
driver_scraping = RunningTest() 
process.crawl(driver_scraping, **kwargs)
process.start()

print 'It took', time.time()-start, 'seconds.'