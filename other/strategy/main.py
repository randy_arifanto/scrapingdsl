class FlyWithRocket():
    def __init__(self):
        pass
    def fly(self):
        print 'FLying with rocket'

class FlyWithWings():
    def __init__(self):
        pass
    def fly(self):
        print 'FLying with wings'

class CantFly():
    def __init__(self):
        pass
    def fly(self):
        print 'I Cant fly'

class SuperDuck:
    def __init__(self, fly_obj):
        self.fly_obj = fly_obj
    def perform_fly(self):
        self.fly_obj.fly()

if __name__ == '__main__':
    fly_behaviour = FlyWithRocket()
    duck = SuperDuck(fly_behaviour)
    #fly_behaviour = FlyWithWings()
    # duck.setFlyingBehaviour(fly_behaviour)
    duck.perform_fly()